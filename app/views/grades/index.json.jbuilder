json.array!(@grades) do |grade|
  json.extract! grade, :id, :Mark, :MarkOutOf
  json.url grade_url(grade, format: :json)
end
