class CreateGrades < ActiveRecord::Migration
  def change
    create_table :grades do |t|
      t.float :Mark
      t.float :MarkOutOf

      t.timestamps null: false
    end
  end
end
